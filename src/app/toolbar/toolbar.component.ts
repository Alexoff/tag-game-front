import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../redux/app.state';
import {CheckWinAction, MixUpAction, SolveTagAction} from '../redux/cubes.action';
import {Cube} from '../redux/cube.model';

@Component({
  selector: 'app-tool',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss', '../global-style/global.scss']
})
export class ToolbarComponent implements OnInit {
  public win: boolean;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit(): void {
    this.store.select('cubePage').subscribe(({win}) => {
      this.win = win;
    });
  }

  public checkWin(): boolean {
    this.store.dispatch(new CheckWinAction());
    return this.win;
  }

  public mixUpCubes(num): void {
    this.store.dispatch(new MixUpAction(num));
  }

  solveTag() {
    this.store.dispatch(new SolveTagAction());
  }

}
