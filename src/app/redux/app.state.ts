import {Cube} from './cube.model';

export interface AppState {
    cubes: Cube[];
    win: boolean;
}
