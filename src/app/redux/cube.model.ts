export class Cube {
  constructor(public id: number,
              public YPosition: number,
              public XPosition: number,
              public active: boolean) {}
}
