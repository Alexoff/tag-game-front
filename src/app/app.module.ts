import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {StoreModule} from '@ngrx/store';
import {cubesReducer} from './redux/cubes.reducer';
import {ToolbarComponent} from './toolbar/toolbar.component';
@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent
  ],
  imports: [
    BrowserModule,
    StoreModule.forRoot({cubePage: cubesReducer})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
