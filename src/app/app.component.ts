import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Cube} from './redux/cube.model';
import {Action, Store} from '@ngrx/store';
import {AppState} from './redux/app.state';
import {
  MixUpAction,
  MoveDownAction, MoveFromClickAction,
  MoveLeftAction,
  MoveRightAction,
  MoveUpAction
} from './redux/cubes.action';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss', './global-style/global.scss']
})
export class AppComponent implements OnInit {
  cubes: Cube[];

  constructor(private store: Store<AppState>) {
  }

  ngOnInit(): void {
    this.store.select('cubePage').subscribe(({cubes}) => {
      this.cubes = cubes;
    });
  }

  public moveFromClickCubes(cube: Cube) {
    this.store.dispatch(new MoveFromClickAction(cube));
  }

  public moveRight(): void {
    this.store.dispatch(new MoveRightAction());
  }

  public moveLeft(): void {
    this.store.dispatch(new MoveLeftAction());
  }

  public moveUp(): void {
    this.store.dispatch(new MoveUpAction());
  }

  public moveDown(): void {
    this.store.dispatch(new MoveDownAction());
  }
}
