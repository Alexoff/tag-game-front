import {Action} from '@ngrx/store';
import {Cube} from './cube.model';

export enum CubeActionTypes {
  MixUp = '[Mix] Mix up',
  CheckWin = '[Win] User win',
  SolveTag = '[Solve} Solve tag game',
  MoveFromClick = '[Click] Move from click',
  MoveRigh = '[Move] Move right',
  MoveLeft = '[Move] Move left',
  MoveUp = '[Move] Move up',
  MoveDown = '[Move] Move down',
}

export class MixUpAction implements Action {
  public readonly type = CubeActionTypes.MixUp;

  constructor(public num: number) {
  }
}

export class CheckWinAction implements Action {
  public readonly type = CubeActionTypes.CheckWin;
}

export class SolveTagAction implements Action {
  public readonly type = CubeActionTypes.SolveTag;
}

export class MoveFromClickAction implements Action {
  public readonly type = CubeActionTypes.MoveFromClick;

  constructor(public cube: Cube) {
  }
}

export class MoveRightAction implements Action {
  public readonly type = CubeActionTypes.MoveRigh;
}

export class MoveLeftAction implements Action {
  public readonly type = CubeActionTypes.MoveLeft;
}

export class MoveUpAction implements Action {
  public readonly type = CubeActionTypes.MoveUp;
}

export class MoveDownAction implements Action {
  public readonly type = CubeActionTypes.MoveDown;
}


export type CubesAction =
  MixUpAction |
  CheckWinAction |
  SolveTagAction |
  MoveFromClickAction |
  MoveRightAction |
  MoveLeftAction |
  MoveUpAction |
  MoveDownAction;
