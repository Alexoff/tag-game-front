import {Cube} from './cube.model';
import {CubeActionTypes, CubesAction, MoveRightAction} from './cubes.action';
import {AppState} from './app.state';

const initialState: AppState = {
    cubes: [
      new Cube(1, 1, 1, true),
      new Cube(2, 1, 2, true),
      new Cube(3, 1, 3, true),
      new Cube(4, 1, 4, true),
      new Cube(5, 2, 1, true),
      new Cube(6, 2, 2, true),
      new Cube(7, 2, 3, true),
      new Cube(8, 2, 4, true),
      new Cube(9, 3, 1, true),
      new Cube(10, 3, 2, true),
      new Cube(11, 3, 3, true),
      new Cube(12, 3, 4, true),
      new Cube(13, 4, 1, true),
      new Cube(14, 4, 2, true),
      new Cube(15, 4, 3, true),
      new Cube(16, 4, 4, false)
    ],
    win: true
  }
;

export function cubesReducer(state = initialState, action: CubesAction) {
  switch (action.type) {
    case CubeActionTypes.MixUp:
      mixUp(state.cubes, action.num);
      return {
        ...state
      };
    case CubeActionTypes.CheckWin:
      return {
        ...state,
        win: state.win = checkWin(state.cubes)
      };
    case CubeActionTypes.SolveTag:
      solveTag(state.cubes);
      return {
        ...state,
      };
    case CubeActionTypes.MoveFromClick:
      moveFromClick(state.cubes, action.cube);
      return {
        ...state,
      };
    case CubeActionTypes.MoveRigh:
      right(state.cubes);
      return {
        ...state
      };
    case CubeActionTypes.MoveLeft:
      left(state.cubes);
      return {
        ...state
      };
    case CubeActionTypes.MoveUp:
      up(state.cubes);
      return {
        ...state
      };
    case CubeActionTypes.MoveDown:
      down(state.cubes);
      return {
        ...state
      };
    default:
      return state;
  }
}

export function mixUp(cubes: Cube[], num: number): Cube[] {
  if (num > 30) {
    num *= 10;
  }
  for (let i = 0; i < num; i++) {
    const a = Math.random() * (10);
    if (a > 0 && a < 2.5) {
      right(cubes);
    } else if (a >= 2.5 && a <= 5) {
      left(cubes);
    } else if (a > 5 && a <= 7.5) {
      up(cubes);
    } else if (a > 7.5 && a <= 10) {
      down(cubes);
    }
  }
  return cubes;
}

export function checkWin(cubes: Cube[]): boolean {
  let count = 0;
  for (let i = 0; i < cubes.length - 1; i++) {
    for (let j = i; j < cubes.length - 1; j++) {
      if (cubes[i].id > cubes[j].id) {
        count++;
      }
    }
  }
  if (count === 0) {
    return true;
  } else {
    return false;
  }
}

export function solveTag(cubes: Cube[]): Cube[] {
  return cubes;
}

export function moveFromClick(cubes: Cube[], cube: Cube): Cube[] {
  const pointerObj = searchDisabledPlateObj(cubes);
  let width: number;
  if (pointerObj.XPosition < cube.XPosition && pointerObj.YPosition === cube.YPosition) {
    width = Math.abs(cube.XPosition - pointerObj.XPosition);
    for (let i = 0; i < width; i++) {
      right(cubes);
    }
  } else if (pointerObj.XPosition > cube.XPosition && pointerObj.YPosition === cube.YPosition) {
    width = Math.abs(cube.XPosition - pointerObj.XPosition);
    for (let i = 0; i < width; i++) {
      left(cubes);
    }
  } else if (pointerObj.YPosition > cube.YPosition && pointerObj.XPosition === cube.XPosition) {
    width = Math.abs(cube.YPosition - pointerObj.YPosition);
    for (let i = 0; i < width; i++) {
      up(cubes);
    }
  } else if (pointerObj.YPosition < cube.YPosition && pointerObj.XPosition === cube.XPosition) {
    width = Math.abs(cube.YPosition - pointerObj.YPosition);
    for (let i = 0; i < width; i++) {
      down(cubes);
    }
  }
  return cubes;
}

export function right(cubes: Cube[]): Cube[] {
  const pointer = searchDisabledPlate(cubes);
  for (let i = 0; i < cubes.length; i++) {
    if (i === pointer && cubes[i].XPosition !== 4) {
      cubes[i].XPosition += 1;
      cubes[i + 1].XPosition -= 1;
      const y = cubes[i + 1];
      cubes[i + 1] = cubes[i];
      cubes[i] = y;
    }
  }
  return cubes;
}

export function left(cubes: Cube[]): Cube[] {
  const pointer = searchDisabledPlate(cubes);
  for (let i = 0; i < cubes.length; i++) {
    if (i === pointer && cubes[i].XPosition !== 1) {
      cubes[i].XPosition -= 1;
      cubes[i - 1].XPosition += 1;
      const y = cubes[i - 1];
      cubes[i - 1] = cubes[i];
      cubes[i] = y;
    }
  }
  return cubes;
}

export function up(cubes: Cube[]): Cube[] {
  const pointer = searchDisabledPlate(cubes);
  for (let i = 0; i < cubes.length; i++) {
    if (i === pointer && cubes[i].YPosition !== 1) {
      cubes[i].YPosition -= 1;
      cubes[i - 4].YPosition += 1;
      const y = cubes[i - 4];
      cubes[i - 4] = cubes[i];
      cubes[i] = y;
    }
  }
  return cubes;
}

export function down(cubes: Cube[]): Cube[] {
  const pointer = searchDisabledPlate(cubes);
  for (let i = 0; i < cubes.length; i++) {
    if (i === pointer && cubes[i].YPosition !== 4) {
      cubes[i].YPosition += 1;
      cubes[i + 4].YPosition -= 1;
      const y = cubes[i + 4];
      cubes[i + 4] = cubes[i];
      cubes[i] = y;
    }
  }
  return cubes;
}

export function searchDisabledPlate(cubes: Cube[]): number {
  for (let i = 0; i < cubes.length; i++) {
    if (cubes[i].active === false) {
      return i;
    }
  }
}

export function searchDisabledPlateObj(cubes: Cube[]): Cube {
  for (let i = 0; i < cubes.length; i++) {
    if (cubes[i].active === false) {
      return cubes[i];
    }
  }
}
